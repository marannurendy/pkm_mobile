import React, { useEffect, useState } from "react";
import { TextInput, View, Text, Dimensions, StyleSheet, Alert, Modal, TouchableOpacity, TurboModuleRegistry } from "react-native";
import { RadioButton } from 'react-native-paper'
import CurrencyInput from 'react-native-currency-input'

const dimension = Dimensions.get('screen')

function GroupCollection_Child(props) {
    useEffect(() => {});
    const [item, setItem] = useState(props.item);
    const [modalVisible, setModalVisible] = useState(false)

    const onChange = (text) => {
        let newValue = text;
        setItem(prevState => {
            let newItem = { ...prevState, ClientName: newValue };

            // sync with [arent array]
            // console.log(newValue)

            props.onChange(props.index, newItem);

            return newItem;
        });
    };

    const reasonHandler = (text) => {
        setItem(prevState => {
            let newItem = { ...prevState, reason: text }

            props.onChange(props.index, newItem);
            return newItem;
        })

        // props.onChange(props.index, newItem);
        // return newItem
    }

    const cancleHandler = () => {
        setItem(prevState => {
            let newItem = { ...prevState, reason: null, attendStatus: null, InstallmentAmount: item.angsuran, totalSetor: item.angsuran }

            props.onChange(props.index, newItem);
            return newItem;
        })
        setModalVisible(!modalVisible)
    }

    const subReasonHandler = () => {
        if(item.reason === null) {
            Alert.alert(
                "Perhatian !",
                "Silahkan pilih alasan ketidakhadiran",
                [
                    {
                        text: 'Ok'
                    }
                ]
            )
        }else{
            setModalVisible(false)
        }
    }

    const attendanceHandler = (text) => {
        let newValue = text;

        if(text === "2" || text === "4" || text === "6") {
            setModalVisible(true)
        }

        if(text === "4" || text === "3") {
            setItem(prevState => {
                let newItem = { ...prevState, attendStatus: newValue, InstallmentAmount: "0", titipan: "0", tarikan: "0", totalSetor: "0", reason: 0 };
    
                // sync with [arent array]
                // console.log(newValue)
    
                props.onChange(props.index, newItem);
    
                return newItem;
            });
        }else{
            if(item.InstallmentAmount === "0" && item.titipan === "0" && item.tarikan === "0" && item.totalSetor === "0") {
                setItem(prevState => {
                    let newItem = { ...prevState, attendStatus: newValue, InstallmentAmount: item.angsuran, totalSetor: item.angsuran, reason: null };
        
                    // sync with [arent array]
                    // console.log(newValue)
        
                    props.onChange(props.index, newItem);
        
                    return newItem;
                });
            }else{
                setItem(prevState => {
                    let newItem = { ...prevState, attendStatus: newValue, reason: null };
        
                    // sync with [arent array]
                    // console.log(newValue)
        
                    props.onChange(props.index, newItem);
        
                    return newItem;
                });
            }
        }
        
        setItem(prevState => {
            let newItem = { ...prevState, attendStatus: newValue };

            // sync with [arent array]
            // console.log(newValue)

            props.onChange(props.index, newItem);

            return newItem;
        });
    };

    const angsuranHandler = (text) => {
        let newValue = text;
        let Total = text + item.titipan - item.tarikan
        setItem(prevState => {
            let newItem = { ...prevState, InstallmentAmount: newValue, totalSetor: Total };

            // sync with [arent array]
            // console.log(newValue)

            props.onChange(props.index, newItem);
            // totalHandler()

            return newItem;
        });
    };

    const setoranHandler = (text) => {
        let newValue = text;
        let Total = Number(item.InstallmentAmount) + text - item.tarikan
        console.log('ini ' + Total)
        setItem(prevState => {
            let newItem = { ...prevState, titipan: newValue, totalSetor: Total };

            // sync with [arent array]
            // console.log(newValue)

            props.onChange(props.index, newItem);

            return newItem;
        });
    };

    const tarikanHandler = (text) => {
        let newValue = text;
        let Total = Number(item.InstallmentAmount) + item.titipan - text
        setItem(prevState => {
            let newItem = { ...prevState, tarikan: newValue, totalSetor: Total };

            // sync with [arent array]
            // console.log(newValue)

            props.onChange(props.index, newItem);

            return newItem;
        });
    };

    return (
        <View style={[styles.container, { backgroundColor: item.sisaKaliAngsuran == 3 ? '#F6E2A7' : item.sisaKaliAngsuran == 2 ? '#F6E2A7' : item.sisaKaliAngsuran == 1 ? '#EDABAC' : '#FFF' }]}>

            <Modal
                animationType='slide'
                transparent={true}
                visible={modalVisible}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#FFF' }}>Pilih alasan ketidakhadiran :</Text>
                        <View style={{ marginTop: 20 }}>
                            <View style={styles.reasonRadio}>
                                <RadioButton 
                                    value= "1"
                                    status={ item.reason === '1' ? 'checked' : 'unchecked'}
                                    onPress={() => reasonHandler('1')} 
                                    uncheckedColor='#FFF'
                                    color="#FFF"
                                />
                                <Text style={{ flex: 1, fontSize: 15, color: '#FFF' }}>Keluarga inti sakit (Orang tua, Suami, sdr sekandung)</Text>
                            </View>
                            <View style={styles.reasonRadio}>
                                <RadioButton 
                                    value= "2"
                                    status={ item.reason === '2' ? 'checked' : 'unchecked'}
                                    onPress={() => reasonHandler('2')}
                                    uncheckedColor='#FFF'
                                    color="#FFF"
                                />
                                <Text style={{ flex: 1, fontSize: 15, color: '#FFF' }}>Menghadiri hajatan keluarga inti/sdr sekandung</Text>
                            </View>
                            <View style={styles.reasonRadio}>
                                <RadioButton 
                                    value= "3"
                                    status={ item.reason === '3' ? 'checked' : 'unchecked'}
                                    onPress={() => reasonHandler('3')}
                                    uncheckedColor='#FFF'
                                    color="#FFF"
                                />
                                <Text style={{ flex: 1, fontSize: 15, color: '#FFF' }}>Menghadiri upacara kegiatan yang menjadi kearifan lokal</Text>
                            </View>
                            <View style={styles.reasonRadio}>
                                <RadioButton 
                                    value= "4"
                                    status={ item.reason === '4' ? 'checked' : 'unchecked'}
                                    onPress={() => reasonHandler('4')}
                                    uncheckedColor='#FFF'
                                    color="#FFF"
                                />
                                <Text style={{ flex: 1, fontSize: 15, color: '#FFF' }}>Cuti melahirkan</Text>
                            </View>
                            <View style={styles.reasonRadio}>
                                <RadioButton 
                                    value= "5"
                                    status={ item.reason === '5' ? 'checked' : 'unchecked'}
                                    onPress={() => reasonHandler('5')}
                                    uncheckedColor='#FFF'
                                    color="#FFF"
                                />
                                <Text style={{ flex: 1, fontSize: 15, color: '#FFF' }}>Nasabah Sakit</Text>
                            </View>
                            <View style={styles.reasonRadio}>
                                <RadioButton 
                                    value= "5"
                                    status={ item.reason === '6' ? 'checked' : 'unchecked'}
                                    onPress={() => reasonHandler('6')}
                                    uncheckedColor='#FFF'
                                    color="#FFF"
                                />
                                <Text style={{ flex: 1, fontSize: 15, color: '#FFF' }}>Alasan lain/Tanpa alasan</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 20 }}>
                            <TouchableOpacity
                                style={{ padding: 5, width: dimension.width/5, alignItems: 'center', borderRadius: 10, backgroundColor: '#E56268' }}
                                onPress={() => {
                                    cancleHandler()
                                }}
                            >
                                <Text style={{ color: '#FFF', fontWeight: 'bold', fontSize: 17 }}>Batal</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{ padding: 5, width: dimension.width/5, alignItems: 'center', borderRadius: 10, backgroundColor: '#4FAD27' }}
                                onPress={() => {
                                    subReasonHandler()
                                }}
                            >
                                <Text style={{ color: '#FFF', fontWeight: 'bold', fontSize: 17 }}>Oke</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>

            <View style={{ margin: 20 }}>
                {/* <TextInput style={{ borderWidth: 1, margin: 5, padding: 5 }} value={item.ClientName} onChangeText={(text) => onChange(text)} /> */}
                <View style={styles.headerCardStat}>
                    <Text numberOfLines={2} style={{fontWeight: 'bold', fontSize: 18, marginBottom: 5, color: '#FAFAF8'}} >{item.ClientName}</Text>
                    <Text style={{fontWeight: 'bold', fontSize: 15, marginBottom: 5, color: '#FAFAF8'}} >{item.ClientID}</Text>
                </View>

                <View style={{marginTop: 10}}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={{marginHorizontal: 10, width: dimension.width/4}}>Product ID</Text>
                        <Text style={{marginHorizontal: 5}}>:</Text>
                        <Text>{item.ProductID}</Text>
                    </View>

                    <View style={{flexDirection: 'row'}}>
                        <Text style={{marginHorizontal: 10, width: dimension.width/4}}>Angsuran Ke</Text>
                        <Text style={{marginHorizontal: 5}}>:</Text>
                        <Text>{item.ke}</Text>
                    </View>
                </View>

                    <View style={{borderBottomWidth: 1, marginVertical: 10}} />

                <View>
                    <Text style={{fontWeight: 'bold', fontSize: 15}}>Kehadiran Nasabah</Text>
                    <View>
                        <View style={styles.RadioStyle}>
                            <RadioButton 
                                value= "1"
                                status={ item.attendStatus === '1' ? 'checked' : 'unchecked'}
                                onPress={() => attendanceHandler('1')} 
                            />
                            <Text>1. Hadir, Bayar</Text>
                        </View>
                        <View style={styles.RadioStyle}>
                            <RadioButton 
                                value= "2"
                                status={ item.attendStatus === '2' ? 'checked' : 'unchecked'}
                                onPress={() => attendanceHandler('2')} 
                            />
                            <Text>2. Tidak Hadir, Bayar</Text>
                        </View>
                        <View style={styles.RadioStyle}>
                            <RadioButton 
                                value= "3"
                                status={ item.attendStatus === '3' ? 'checked' : 'unchecked'}
                                onPress={() => attendanceHandler('3')} 
                            />
                            <Text>3. Hadir, Tidak Bayar</Text>
                        </View>
                        <View style={styles.RadioStyle}>
                            <RadioButton 
                                value= "4"
                                status={ item.attendStatus === '4' ? 'checked' : 'unchecked'}
                                onPress={() => attendanceHandler('4')} 
                            />
                            <Text>4. Tidak Hadir, Tidak Bayar</Text>
                        </View>
                        <View style={styles.RadioStyle}>
                            <RadioButton 
                                value= "5"
                                status={ item.attendStatus === '5' ? 'checked' : 'unchecked'}
                                onPress={() => attendanceHandler('5')} 
                            />
                            <Text>5. Hadir, Tanggung Renteng</Text>
                        </View>
                        <View style={styles.RadioStyle}>
                            <RadioButton
                                value= "6"
                                status={ item.attendStatus === '6' ? 'checked' : 'unchecked'}
                                onPress={() => attendanceHandler('6')} 
                            />
                            <Text>6. Tidak Hadir, Tanggung Renteng</Text>
                        </View>
                    </View>
                </View>

                <View style={{borderBottomWidth: 1, marginVertical: 10}} />

                <View>
                    <Text style={styles.Detailtitle}>Pembayaran*</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{fontSize: 16, flex: 3, fontWeight: 'bold'}}>Angsuran</Text>
                        <View style={{ borderWidth: 1, flex: 2, paddingLeft: 5, borderRadius: 10 }} >
                            <CurrencyInput
                            // onChangeText
                                // onBlur={(val) => angsuranHandler(val, index)}
                                onChangeValue={(val) => angsuranHandler(val)}
                                value={item.attendStatus == '3' ? 0 : item.attendStatus == '4' ? 0 : item.InstallmentAmount}
                                defaultValue={"1"}
                                prefix="Rp "
                                delimiter=","
                                separator="."
                                precision={0}
                                editable={item.attendStatus == '3' ? false : item.attendStatus == '4' ? false : item.attendStatus == '7' ? false : true}
                                style={{ fontSize: 15 }}
                            />
                        </View>
                    </View>

                    <View style={{ alignItems: 'flex-end' }}>
                        <View style={styles.deviderStyle} />
                    </View>

                    <View> 
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Text>Saldo Titipan Saat ini : </Text>
                            <CurrencyInput
                                value={item.VolSavingsBal}
                                prefix="Rp "
                                delimiter=","
                                separator="."
                                precision={0}
                                editable={false}
                                style={{
                                    color: 'black'
                                }}
                                // onChangeText={this.Hitung(this.state.angsuran[idx])}
                            />
                        </View>
                    </View>
                    <Text style={{ fontSize: 16, paddingTop: 20, fontWeight: 'bold' }}>Titipan</Text>
                    <View style={{flex: 1}}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginVertical: 5 }}>
                            <Text style={{ flex: 3, marginLeft: 20 }}>Setoran</Text>
                            <View style={{ borderWidth: 1, paddingLeft: 5, borderRadius: 10, flex: 2 }}>
                                <CurrencyInput
                                    onChangeValue={(text) => setoranHandler(text)}
                                    value={item.attendStatus == '4' ? 0 : item.titipan}
                                    prefix="Rp "
                                    delimiter=","
                                    separator="."
                                    precision={0}
                                    editable={item.attendStatus == '4' ? false : item.attendStatus == '7' ? false : true}
                                    // onChangeText={this.Hitung(this.state.angsuran[idx])}
                                />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginVertical: 5 }}>    
                            <Text style={{ flex: 3, marginLeft: 20 }}>Tarikan</Text>
                            <View style={{ borderWidth: 1, paddingLeft: 5, borderRadius: 10, flex: 2 }}>
                                <CurrencyInput
                                    onChangeValue={(text) => tarikanHandler(text)}
                                    value={item.attendStatus == '4' ? 0 : item.tarikan}
                                    prefix="Rp "
                                    delimiter=","
                                    separator="."
                                    maxValue={item.VolSavingsBal}
                                    precision={0}
                                    editable={item.attendStatus == '4' ? false : item.attendStatus == '7' ? false : true}
                                    // onChangeText={this.Hitung(this.state.angsuran[idx])}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={{ alignItems: 'flex-end' }}>
                        <View style={styles.deviderStyle} />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingTop: 20 }}>
                        <Text style={{ fontSize: 16, flex: 3, fontWeight: 'bold' }}>Jumlah Setor</Text>
                        <View style={{ borderWidth: 1, paddingLeft: 5, borderRadius: 10, flex: 2 }}>
                            <CurrencyInput
                                // onChangeValue={(text) => this.handleChangeInput(text, idx, dt.AccountID, 'total')}
                                value={item.attendStatus == '4' ? 0 : item.totalSetor}
                                prefix="Rp "
                                delimiter=","
                                separator="."
                                precision={0}
                                editable= {false}
                                style={{
                                    color: 'black'
                                }}
                            />
                        </View>
                    </View>
                </View>

            </View>
        </View>
    );
}

export default GroupCollection_Child

const styles = StyleSheet.create({
    container : {
        marginVertical: 10,
        borderRadius: 20,
        // backgroundColor: '#FFF',
        flex: 1
    },
    centeredView : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalView : {
        height: dimension.height/2,
        width: dimension.width/1.3,
        backgroundColor: '#2899D7',

        borderColor: '#FFF',
        borderRadius: 20,
        padding: 20
    },
    headerCardStat : {
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius:20,
        backgroundColor: '#0E71C4'
    },
    reasonRadio: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 5
    },
    RadioStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    deviderStyle: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        width: dimension.width/1.5,
        padding: 10,
    },
})
